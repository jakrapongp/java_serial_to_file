package com.allvet;

import com.fazecast.jSerialComm.SerialPort;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

public class Main {

    static HashMap<String, BufferedWriter> fileHandler = new HashMap<String, BufferedWriter>();

    static int baudrate = 9600;
    static String comPort = "cu.usbmodem14101";

    public static void main(String[] args) {

        if(args.length != 2){
            System.out.println("*****************************************************************");
            System.out.println("Usage arguments : java -jar xxx.jar portname baudrate");
            System.out.println("*****************************************************************");
            System.out.println("Available ports: ");
            for (SerialPort p:SerialPort.getCommPorts()) {
                System.out.println(p.getSystemPortName());
            }
            System.out.println("*****************************************************************");
            System.exit(0);
        }
        comPort = args[0];
        baudrate = Integer.parseInt(args[1]);

        SerialPort port = SerialPort.getCommPort(comPort);
        port.setBaudRate(baudrate);

        if(port.openPort()) {
            port.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 0, 0);
            InputStream in = port.getInputStream();

            SimpleDateFormat d1 = new SimpleDateFormat("YYYY-MM-DD");
            SimpleDateFormat d2 = new SimpleDateFormat("YYYY-MM-DD HH:mm:ss");
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                while (true) {
                    while (reader.ready()) {
                        String line = reader.readLine();

                        String[] datas = line.split(",");
                        String id = datas[0];
                        Date date = Calendar.getInstance().getTime();

                        String dateString = d1.format(date);
                        String timeStamp = d2.format(date);

                        String fileName = id + "_" + dateString + ".txt";

                        String logData = timeStamp + ": " + line;
                        System.out.println(logData);
                        writeFile(fileName, logData);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                port.closePort();

                Set<String> keys = fileHandler.keySet();
                for (String k : keys) {
                    BufferedWriter writer = fileHandler.get(k);
                    try {
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }else{
            System.out.println("*****************************************************************");
            System.out.println("Unable to open port : " + comPort);
            System.out.println("*****************************************************************");
            System.out.println("Available ports: ");
            for (SerialPort p:SerialPort.getCommPorts()) {
                System.out.println(p.getSystemPortName());
            }
            System.out.println("*****************************************************************");

        }

    }

    public static void writeFile(String filename, String string){

        BufferedWriter writer = null;

        if(fileHandler.containsKey(filename)){
            writer = fileHandler.get(filename);
        }else {
            try {
                writer = new BufferedWriter(new FileWriter(filename, true));
                fileHandler.put(filename,writer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            writer.write('\n');
            writer.write(string);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
